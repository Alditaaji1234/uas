import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

void main() {
  runApp(NoteApp());
}

class NoteApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Note App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: NoteListPage(),
    );
  }
}

class NoteListPage extends StatefulWidget {
  @override
  _NoteListPageState createState() => _NoteListPageState();
}

class _NoteListPageState extends State<NoteListPage> {
  List<Note> notes = [];

  @override
  void initState() {
    super.initState();
    loadNotes();
  }

  Future<void> loadNotes() async {
    try {
      Directory directory = await getApplicationDocumentsDirectory();
      File file = File('${directory.path}/notes.json');

      if (await file.exists()) {
        String fileContent = await file.readAsString();
        List<dynamic> jsonNotes = json.decode(fileContent);
        setState(() {
          notes = jsonNotes.map((note) => Note.fromJson(note)).toList();
        });
      }
    } catch (e) {
      print('Failed to load notes: $e');
    }
  }

  Future<void> saveNotes() async {
    try {
      Directory directory = await getApplicationDocumentsDirectory();
      File file = File('${directory.path}/notes.json');
      String fileContent = json.encode(notes);

      await file.writeAsString(fileContent);
    } catch (e) {
      print('Failed to save notes: $e');
    }
  }

  void addNote() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditNotePage(),
      ),
    ).then((value) {
      if (value != null) {
        setState(() {
          notes.add(value);
        });
        saveNotes();
      }
    });
  }

  void editNote(int index) {
    Note currentNote = notes[index];
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditNotePage(note: currentNote),
      ),
    ).then((value) {
      if (value != null) {
        setState(() {
          notes[index] = value;
        });
        saveNotes();
      }
    });
  }

  void deleteNote(int index) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Delete Note'),
        content: Text('Are you sure you want to delete this note?'),
        actions: [
          TextButton(
            child: Text('Cancel'),
            onPressed: () => Navigator.pop(context),
          ),
          TextButton(
            child: Text('Delete'),
            onPressed: () {
              setState(() {
                notes.removeAt(index);
              });
              saveNotes();
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Note List'),
      ),
      body: ListView.builder(
        itemCount: notes.length,
        itemBuilder: (context, index) {
          Note note = notes[index];
          return Card(
            child: ListTile(
              title: Text(note.title),
              subtitle: Text(note.timestamp.toString()),
              onTap: () => editNote(index),
              onLongPress: () => deleteNote(index),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: addNote,
        child: Icon(Icons.add),
      ),
    );
  }
}

class EditNotePage extends StatefulWidget {
  final Note? note;

  const EditNotePage({Key? key, this.note}) : super(key: key);

  @override
  _EditNotePageState createState() => _EditNotePageState();
}

class _EditNotePageState extends State<EditNotePage> {
  late TextEditingController _titleController;
  late TextEditingController _contentController;

  @override
  void initState() {
    super.initState();
    _titleController = TextEditingController(text: widget.note?.title);
    _contentController = TextEditingController(text: widget.note?.content);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.note != null ? 'Edit Note' : 'Add Note'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              controller: _titleController,
              decoration: InputDecoration(
                labelText: 'Title',
              ),
            ),
            SizedBox(height: 16.0),
            TextField(
              controller: _contentController,
              maxLines: null,
              decoration: InputDecoration(
                labelText: 'Content',
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          String title = _titleController.text;
          String content = _contentController.text;
          Note updatedNote = Note(
            title: title,
            content: content,
            timestamp: DateTime.now(),
          );
          Navigator.pop(context, updatedNote);
        },
        child: Icon(Icons.save),
      ),
    );
  }
}

class Note {
  final String title;
  final String content;
  final DateTime timestamp;

  Note({
    required this.title,
    required this.content,
    required this.timestamp,
  });

  factory Note.fromJson(Map<String, dynamic> json) {
    return Note(
      title: json['title'],
      content: json['content'],
      timestamp: DateTime.parse(json['timestamp']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'content': content,
      'timestamp': timestamp.toIso8601String(),
    };
  }
}